#!/usr/bin/env bash

# Don't duplicate lines in history (force ignoredups and ignorespace).
# See bash(1) for more options
HISTCONTROL=ignoreboth:erasedups

# Append history to file, don't overwrite
shopt -s histappend

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=1000
HISTFILESIZE=2000

# Add ~/.local/bin directory to $PATH
if [ -d "$HOME/.local/bin" ] && [[ ":$PATH:" != *":$HOME/.local/bin:"* ]]; then
  export PATH="$HOME/.local/bin:$PATH";
fi

# Add Ruby directories to $PATH
if [ -d "$HOME/.rbenv/bin" ] && [[ ":$PATH:" != *":$HOME/.rbenv/bin:"* ]]; then
  export PATH="$HOME/.rbenv/bin:$PATH"
  eval "$(rbenv init -)"
  export PATH="$HOME/.rbenv/plugins/ruby-build/bin:$PATH"
fi

# Add Python directories to $PATH
# if [ -d "$HOME/.pyenv/bin" ] && [[ ":$PATH:" != *":$HOME/pyenv/bin:"* ]]; then
#   export PATH="$HOME/.pyenv/bin:$PATH"
#   if command -v pyenv 1>/dev/null 2>&1; then
#     eval "$(pyenv init -)"
#   fi
# fi

# Add NPM directory to $PATH
if [ -d "$HOME/.npm/bin" ] && [[ ":$PATH:" != *":$HOME/.npm/bin:"* ]]; then
  export PATH="$HOME/.npm/bin:$PATH"
fi

# WP CLI Auto Complete
# if [ -f "$HOME/.wp-completion.bash" ] && [[ ":$PATH:" != *":$HOME/.wp-completion.bash:"* ]]; then
#   # shellcheck source=/home/brad/
#   source "$HOME/.wp-completion.bash"
# fi

# Alias definitions
if [ -f "$HOME/.bash_aliases" ] && [[ ":$PATH:" != *":$HOME/.bash_aliases:"* ]]; then
  # shellcheck source=/home/brad/
  source "$HOME/.bash_aliases"
fi

# Functions
if [ -f "$HOME/.functions" ] && [[ ":$PATH:" != *":$HOME/.functions:"* ]]; then
  # shellcheck source=/home/brad/
  source "$HOME/.functions"
fi

# Git enabled bash-prompt
if [ -f "$HOME/.bash_prompt" ] && [[ ":$PATH:" != *":$HOME/.bash_prompt:"* ]]; then
  # shellcheck source=/home/brad/
  source "$HOME/.bash_prompt"
fi

# Enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
if [ -f /etc/bash_completion ] && ! shopt -oq posix; then
  source /etc/bash_completion
fi
