#!/usr/bin/env bash

alias rebash='source ~/.bashrc'

alias ls='ls --color=auto'
alias ll='ls --almost-all -l --human-readable --color=auto'
# https://news.ycombinator.com/item?id=35839926

alias dot="code ~/.dotfiles"

# Editor Shortcuts
alias a="code .;" # It's been a long while since I've used my beloved Atom but I still type 'a' on occation when I meant to type 'c'.
alias c="code .;"
alias s="subl .;"

# Project directories
alias brad="cd ~/projects/bradonomics.com"
alias jfg="cd ~/projects/jekyll-field-guide"

# Nebo Project directories
alias nebo="cd ~/projects/nebo"
alias ashton="cd ~/projects/nebo/ashton"
alias starlight="cd ~/projects/nebo/starlight"
alias aptos="cd ~/projects/nebo/aptos"
alias revionics="cd ~/projects/nebo/revionics"

# Git shortcuts
alias stash="git stash --include-untracked"
alias pop="git stash pop"
alias fetch="git fetch --all"

# APT shortcuts
alias update='sudo apt update'
alias upgrade='sudo apt update && sudo apt -y upgrade'
alias install='sudo apt install'
alias remove='sudo apt remove'

# Other shortcuts
alias ph='portholes'
alias notes='write'
alias log='write work'

# Services
# alias start-mongo='sudo service mongod start'

# Delete Rails server logs
alias dev_logs='> log/development.log'
alias dev_log='dev_logs'
alias clear_logs='dev_logs'

# Make echo $PATH readable
alias path='echo $PATH | tr ":" "\n"'

# ipconfig-ish equivalent
alias ipconfig='nmcli device show eno1 | grep IP4'

# A ipconfig /flushdns equivalent
alias flushdns='sudo /etc/init.d/dns-clean restart && /etc/init.d/networking force-reload'
alias dnsflush='flushdns'

# Delete those stupid Mac .DS_Store files
alias delete-ds="find . -type f -name '.DS_Store' -ls -delete"

# Get External IP
alias ip='curl icanhazip.com'

# Get Local IPs
alias iplocal="ifconfig | grep 'inet '"

# Jekyll clean, build, and serve commands, with much wow
alias clean='bundle exec jekyll clean'
alias build='bundle exec jekyll build'
# alias serve='bundle exec jekyll serve --livereload'
# alias serve='parallelshell "bundle exec jekyll serve --livereload" "~/.local/share/firefox_dev/firefox 'http://localhost:4000/'"'
# alias serve='parallelshell "bundle exec jekyll serve --livereload" "google-chrome 'http://localhost:4000/'"'
alias serve-bs='parallelshell "bundle exec jekyll build --watch" "browser-sync start --server '_site' --files '_site'"'

# Jekyll publish/deploy
alias publish='deploy'

# Sphinx horseshit
alias sphinx='bundle exec rake ts:stop && bundle exec rake ts:clear && bundle exec rake ts:configure && bundle exec rake ts:start'
